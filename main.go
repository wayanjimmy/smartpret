package main

import (
	"flag"
	"fmt"
	"os"
	"regexp"

	"net/http"
	"net/url"

	"github.com/PuerkitoBio/goquery"
)

var config bool

func init() {
	flag.BoolVar(&config, "config", false, "Make config file.")
	flag.BoolVar(&config, "c", false, "Make config file.")
}

func getQuota() {
	baseURL := "https://my.smartfren.com/api/device/profile.php"
	resp, err := http.PostForm(baseURL, url.Values{"imsi": {os.Getenv("SMARTFREN_IMSI")}, "token": {os.Getenv("SMARTFREN_TOKEN")}})
	if err != nil {
		fmt.Println("Something wrong...")
		return
	}
	defer resp.Body.Close()

	doc, err := goquery.NewDocumentFromResponse(resp)
	if err != nil {
		fmt.Println(err)
		return
	}
	doc.Find("table").Each(func(i int, s *goquery.Selection) {
		if i == 1 {
			table := s.Find("tr").Find("td").Text()
			r, _ := regexp.Compile("Sisa Kuota(.*)MB")

			fmt.Printf("%d: %s\n", i, r.FindString(table))
		}
	})
}

func main() {
	flag.Parse()
	if config == true {
		fmt.Println("Setup config")
	} else {
		fmt.Println("Crawl kuota info")
		getQuota()
	}
}
