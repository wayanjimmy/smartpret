# Smartpret
Antarmuka perintah terminal untuk cek kuota Smartfren

## Cara install
1. Install [Go](https://golang.org)
2. Clone `smartpret` ke dalam `$GOPATH`
3. `$ cd smartpret`
4. `$ go get .`
5. `$ go install .`
6. Tambahkan `SMARTFREN_TOKEN` dan `SMARTFREN_IMSI` ke `~/.bashrc` atau `~/.zshrc`
7. Jalankan dengan perintah `smartpret`

## Cara setup ENV var di `~/.bashrc`
```
export SMARTFREN_IMSI=12345
export SMARTFREN_TOKEN=abcde
```
restart terminal atau jalankan `source ~/.bashrc`
